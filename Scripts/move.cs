using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class move : MonoBehaviour
{

    int direction_x=1;
    int direction_y=1;

    int max_x=2;
    int min_x=-2;
    int max_y=2;
    int min_y=0;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (this.transform.position.x>max_x){direction_x=-1;}
        if (this.transform.position.x<min_x){direction_x=1;}
        if (this.transform.position.y>max_y){direction_y=-1;}
        if (this.transform.position.y<min_y){direction_y=1;}
        float new_x=this.transform.position.x + direction_x*0.01f;
        float new_y=this.transform.position.y + direction_y*0.005f;
        this.transform.position=new Vector3(new_x,new_y,this.transform.position.z);

    }
}
