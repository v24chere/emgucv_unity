using UnityEngine;
using Emgu.CV;
using Emgu.CV.CvEnum;
using System.Drawing;
using System.Runtime.InteropServices;
using System.Threading;
using System.Collections.Concurrent;

// Script recording the camera cameraToRecord in outputFilePath
//
public class EmguCV_Recorder : MonoBehaviour
{
    public Camera cameraToRecord;
    public string outputFilePath = "Assets/output.avi";
    //public string outputFilePath = "Assets/output.mp4";
    private int frameWidth;
    private int frameHeight;
    public int frameRate = 30;


    public bool playing = true;

    private VideoWriter videoWriter;
    private RenderTexture renderTexture;
    private Texture2D screenTexture;
    private Thread recordingThread;
    private ConcurrentQueue<Color32[]> frameQueue;




    private void Start()
    {
        // Initialize video writer
        frameWidth = Screen.width;
        frameHeight = Screen.height;

        // record in .avi
        videoWriter = new VideoWriter(outputFilePath, VideoWriter.Fourcc('M', 'J', 'P', 'G'), frameRate, new System.Drawing.Size(frameWidth, frameHeight), true);
        // record in .mp4
        //videoWriter = new VideoWriter(outputFilePath, VideoWriter.Fourcc('X', '2', '6', '4'), frameRate, new Size(frameWidth, frameHeight), true);

        if (!videoWriter.IsOpened)
        {
            Debug.LogError("Failed to open video writer.");
            return;
        }

        // Create a RenderTexture and a Texture2D for capturing camera output
        renderTexture = new RenderTexture(frameWidth, frameHeight, 24);
        screenTexture = new Texture2D(frameWidth, frameHeight, TextureFormat.RGB24, false);

        // Initialize the frame queue
        frameQueue = new ConcurrentQueue<Color32[]>();

        // Start the recording thread
        recordingThread = new Thread(new ThreadStart(RecordVideo));
        recordingThread.Start();

        // Start capturing frames
        //isRecording = true;
    }


    private void Update(){
        if (playing)
            {CaptureFrame();}
    }


    private void CaptureFrame()
    {
        // Capture the camera output to the RenderTexture
        cameraToRecord.targetTexture = renderTexture;
        cameraToRecord.Render();
        cameraToRecord.targetTexture = null;

        // Read the RenderTexture into the Texture2D
        RenderTexture.active = renderTexture;
        screenTexture.ReadPixels(new Rect(0, 0, frameWidth, frameHeight), 0, 0);
        screenTexture.Apply();
        RenderTexture.active = null;

        // Enqueue the captured frame
        frameQueue.Enqueue(screenTexture.GetPixels32());
    }
    public void end_record(){
        playing=false;
        // Wait for the recording thread to finish
        if (recordingThread != null) // It should be in OnApplicationQuit but
        {                             //in the editor if you stop the test it won't allow the thread to finish causing a crash
            recordingThread.Join();
        }
        OnApplicationQuit();
    }
    private void OnApplicationQuit()
    {



        if (videoWriter != null)
        {
            videoWriter.Dispose();
        }

        // Release resources
        if (renderTexture != null)
        {
            renderTexture.Release();
            Destroy(renderTexture);
        }
        if (screenTexture != null)
        {
            Destroy(screenTexture);
        }
    }
    void RecordVideo()
    {
        while (playing || !frameQueue.IsEmpty)
        {
            if (frameQueue.TryDequeue(out Color32[] pixelData))
            {
                // we allocate some memory to manipulate the screenshot
                GCHandle handle = GCHandle.Alloc(pixelData, GCHandleType.Pinned);

                 using (Mat frame = new Mat(frameHeight, frameWidth, DepthType.Cv8U, 4, handle.AddrOfPinnedObject(), frameWidth * 4))
                {
                    // Convert BGRA to BGR
                    using (Mat bgrFrame = new Mat())
                    {
                        CvInvoke.CvtColor(frame, bgrFrame, ColorConversion.Bgra2Bgr);
                        CvInvoke.CvtColor(bgrFrame, bgrFrame, ColorConversion.Bgr2Rgb);

                        // Flip the image vertically
                        CvInvoke.Flip(bgrFrame, bgrFrame, FlipType.Vertical);

                        // Write the frame to the video
                        videoWriter.Write(bgrFrame);
                    }
                }
                // we release the memory we allocated
                handle.Free();
            }
            else
            {
                // If there are no frames in the queue, wait briefly before checking again
                Thread.Sleep(10);
            }
        }
    }
}