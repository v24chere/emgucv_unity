# EmguCv_Unity



## Getting started

Hello this is a tutorial to use Open CV on Unity-Windows with the use of Emgu CV. 
We use a CPU version.

This is open source for open source but for commercial uses you'll need to purchase licenses, for more details see : https://www.emgu.com/wiki/index.php/Licensing:#A_Quick_Overview


## Downloads

You can copy the /Plugins/ directory above and put it in your Unity's /Assets/ directory 
(It worked both in a 2021 and 2022 version you can try in other version I don't see a particular reason why it wouldn't work)
Or build it step by step see at the bottom

## Test

Wether you downloaded my /Plugins/ directory or built it step by step, we can now make a few test.

(We'll add a ball and a move script to have something to look at in our blank project see [Scripts/move.cs](Scripts/move.cs))

#### First test: Recording
a real time recording feature see [Scripts/Recorder](Scripts/Recorder.cs). 
Place the script on a gameobject and link it the camera you want to record.   

There is a colorConversion because OpenCv is in BGR while Unity is in RGB.
And a Flip because OpenCv places the origin on the top left of images and Unity in the bottom left (=> going up means an increasing Y_coordinate in Unity but decreasing in Open_Cv)

The script can extend frames duration be sure to use Time.deltaTime in your scripts when needed.
If you have performance issue try recording in .avi format instead of .mp4 (comment/uncomment appropriate lines).

Note that the path by default ( Assets/output.avi ) will only be useful in the editor as /Assets is inaccessible once built.
You can use "Application.persistentDataPath" for a consistent way to read/write outside your Unity Application.


#### Second test: Basic tracking
todo

## Step by Step Unity Install

On this link https://github.com/emgucv/emgucv/releases   
Go find the release 4.9.0 (latest currently) download "libemgucv-windesktop-4.9.0.5494.zip"   

<img src="screenshots/emgucv_release_4_9_0.png" width="550"> 

Unzip it and go into \libs
Make a directory anywhere called Plugins (It'll go in your unity project later)   
Copy all the dynamic link library ( .dll) here into Plugins. 

<img src="screenshots/libemgucv_libs.png" width="550">

Now go into \libs\runtimes\win-x64\native (change win-x64 if you're not in 64 bit)

<img src="screenshots/libemgucv_libs_native.png" width="550"> 

And again copy all the dynamic link library ( .dll) here into Plugins.

### Unity

I didn't test in all version but it worked in my 2021 version's project and
I'm using 2022.3.37f1 version here. We'll start from a blank 3D project (if you can't make it work in your project you can create a blank project quickly and try)

<img src="screenshots/unity_blank3D.PNG" width="550">

In Assets we will (create a Scripts directory for later and )   
copy the Plugins directory. You will have a lot of Dependencies issues :

<img src="screenshots/erreur_drag_plugins.PNG" width="550">

We'll need to go on www.nuget.org and download those dependencies :   
Here I need System.Text.Json , (PresentationCore) , System.Drawing.Common , Microsoft.VisualStudio.DebuggerVisualizers
First download the nupkg and extract it (same as downlading a .zip and unzip)

<img src="screenshots/ex_nuget_pack.PNG" width="550"> 

Then inside we go to lib/netstandard2.0 and copy the .dll to Plugins

<img src="screenshots/inside_nupkg.PNG" width="550">

We keep downloading until all dependencies are resolved (note that some dependencies will have dependencies of their own).   
I deleted Emgu.CV.Wpf in Plugins because I couldn't find his dependencies PresentationCore properly. 
It's a minor part of EmguCv we should still be able to use the majority of it. Same for Netcore and Netframework.